# Programming test

## Installing dependencies

sudo apt install python-pip
sudo pip install -r requirements.txt

## Running the server

The server has two modes: one to boostrap the files and one to serve them.

### Boostrapping

At the root level:

```python -m server.main --server-folder <server_folder> --bootstrap-folder <boostrap-folder> --server-dictionary <server_dictionary>```

This mode will copy all files from boostrap-folder to server-folder, generating the server dictionary on the meanwhile.

### Serving
At the root level:

```python -m server.main --server-dictionary <server_dictionary>```

This mode will serve the files based on the server_dictionary.

## Running the client

At the root level:

```python -m client.main --sync-dir <sync_dir> --server-address http://localhost:5000 --dictionary-file <dictionary_file>```

This will synchronize the dictionary_file (or create a new one if needed) with the files on the server


# How it works

The boostrap generates the server dictionary, and upon running a client can request the files from the server, uploading his own
client dictionary. The server then computes which files are different from the client, and returns them with the diff xml as a multipart
response. The client then merges the response, and rewrites the files on the same request. This prevents async problems at server side
and simplifies the client. If the server has nothing to change it returns a http 304.

Missing:
    * test at API level on the client and on the server
    * the client can be better structured


