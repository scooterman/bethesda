# -*- coding: utf-8 -*-
# Copyright tracktoor. All rights reserved.
# 18/03/18
# @author: Victor Vicente de Carvalho
from flask import Flask, request, current_app, Response
from requests_toolbelt import MultipartEncoder
from lxml import etree
from . import controller

app = Flask(__name__)


@app.route('/sync', methods=['POST'])
def sync():
    if 'xml' in request.files:
        client_xml = request.files['xml']
        xml = client_xml.read()
        dom_response, files = controller.get_client_xml_and_files(xml, current_app.server_xml)
    else:
        dom_response, files = controller.get_client_xml_and_files(None, current_app.server_xml)

    if not files:
        return Response(status=304)

    files_dict = {'xml': ('xml', etree.tostring(dom_response), 'text/xml')}
    files_dict.update({fdesc.name: (fdesc.name, open(fdesc.path, 'rb'), 'binary/octetstream') for fdesc in files})
    encoder = MultipartEncoder(fields=files_dict)

    return Response(encoder.to_string(), mimetype=encoder.content_type)
