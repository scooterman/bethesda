import os
import shutil
from collections import namedtuple

# simple asbraction for the name/path tuple
# for non pythoneers: this is basically an optimized class with two variables
FileDesc = namedtuple('FileDesc', ['name', 'path'])


def build_files_list(path, result):
    for root, dirs, files in os.walk(path):
        for name in files:
            result.append(FileDesc(name, os.path.join(root, name)))

        for fdir in dirs:
            build_files_list(fdir, result)


def copy_file(src, dst):
    shutil.copy(src, dst)


def file_length(filename):
    statinfo = os.stat(filename)
    return statinfo.st_size
