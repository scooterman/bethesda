# -*- coding: utf-8 -*-
# Copyright tracktoor. All rights reserved.
# 18/03/18
# @author: Victor Vicente de Carvalho
import os
import sys
import requests
import copy
import logging

from requests_toolbelt.multipart import decoder
from argparse import ArgumentParser
from lxml import etree

logger = logging.getLogger('main')


def extract_filename(content_disposition):
    return content_disposition.split(';')[-1].split('=')[1].replace('"', '')


def write_file(filename, contents):
    with open(filename, 'wb') as f:
        f.write(contents)


def merge_doms(root, diff):
    for document in diff.xpath('./document'):
        found = root.xpath('./document/document_name[text()="{document_name}"]/..'.format(
            document_name=document.find('document_name').text))

        if found:
            root.remove(found[0])

        root.append(copy.copy(document))


def main(sync_dir, dictionary_path, server_address):
    if not os.path.isdir(sync_dir):
        raise ValueError('Sync dir has to be a directory')

    files = None
    dictionary_tree = None

    if os.path.exists(dictionary_path):
        files = {'xml': open(dictionary_path, 'rb')}
        dictionary_tree = etree.parse(dictionary_path)

    response = requests.post(server_address + '/sync', files=files)

    if response.status_code != 200:
        if response.status_code == 304:
            logger.info('Server contents are identical. No need to synchronize')
            return

        logger.exception('Server returned with status {status} '
                         'for sync operation'.format(status=response.status_code))
        sys.exit(-1)

    multipart_data = decoder.MultipartDecoder.from_response(response)

    for part in multipart_data.parts:
        filename = extract_filename(part.headers['content-disposition'])
        if filename == 'xml':
            response_dom = etree.fromstring(part.content)

            merge_doms(dictionary_tree.getroot(), response_dom)

            dictionary_tree.write(dictionary_path)
        else:
            write_file(sync_dir + '/{document_name}'.format(document_name=filename), part.content)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    arguments = ArgumentParser('bethesda-cli')
    arguments.add_argument('--sync-dir', required=True, help='the directory where to sync the files')
    arguments.add_argument('--dictionary-file', required=True,
                           help='the client dictionary file containing the versions')
    arguments.add_argument('--server-address', required=True, help='The server address')

    args = arguments.parse_args()

    main(args.sync_dir, args.dictionary_file, args.server_address)
